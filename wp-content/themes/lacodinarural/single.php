<?php get_header(); ?>

<main>
	<div id="noticia_single" class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
		<?php
		if(have_posts()): while(have_posts()): the_post(); ?>
		<div class="noticia">
			<span class="date sans"><?= get_the_date('j - n - Y'); ?></span>
			<h1><?php the_title();?></h1>
			<?php $images = get_field('slider'); if( $images ): ?>
				<div class="post-slider">
					<?php foreach( $images as $image ): ?>
						<img src="<?php echo $image['sizes']['slider-news']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			<div class="post-content">
				<?php the_field('content'); ?>
			</div>
		</div>
		<?php endwhile; endif; ?>
	</div>
</main>

<?php get_footer(); ?>

<?php
/*Template Name: La casa*/
get_header(); ?>

<main>
	<?php/*<div class="container">
		<div class="content-header">
			<h1><?php the_title(); ?></h1>
			<?php //menuChildrenPages(); ?>
		</div>
	</div>
	<div id="sliderHomeWrap" class="proportion" data-numerador="5" data-denominador="2" data-maxheight="true">
		<span id="titular_slider" class="sans vertical-align"><?php the_field('titular_slider'); ?></span>
		<div id="sliderHome">
			<?php 
			$images = get_field('slider');
			if( $images ): foreach( $images as $image ): ?>
				<?//<img src="<?php echo $image['sizes']['slider-casa']; ?>" alt="<?php echo $image['alt']; ?>" />?>
				<div style="width: 100%; height: 100%; background-image: url('<?php echo $image['sizes']['slider-casa'] ?>'); background-size: cover; background-position: center;"></div>
			<?php endforeach; endif; ?>
		</div>
	</div>*/?>
	<div class="container">
		<div class="content-main">
			<?php //diamonds(); ?>
			<?php//<div id="content" class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12"><?php the_field('content'); ?><?php//</div>?>
			<?php //checkered('left'); ?>
			<?php childrenPages(); ?>
		</div>
	</div>
</main>

<?php get_footer(); ?>

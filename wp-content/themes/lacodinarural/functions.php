<?php
/*
Author: Bisgràfic
URL: http://www.bisgrafic.com
*/

wp_register_script( 'index-js', get_stylesheet_directory_uri() . '/assets/index.js', array( 'jquery' ), '', true );
wp_enqueue_script( 'index-js' );
add_action('admin_head', 'admin_css');
function admin_css() {
  echo '<link rel="stylesheet" href="'.get_stylesheet_directory_uri().'/library/css/admin-style.css" type="text/css" media="all" />';
}

require_once( 'library/bones.php' );

// CUSTOMIZE THE WORDPRESS ADMIN (off by default)
require_once( 'library/admin.php' );

function bones_ahoy() {
  add_editor_style( get_stylesheet_directory_uri() . '/library/css/editor-style.css' );
  load_theme_textdomain( 'bonestheme', get_template_directory() . '/library/translation' );
  //require_once( 'library/custom-post-type.php' );
  add_action( 'init', 'bones_head_cleanup' );
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  add_filter( 'the_generator', 'bones_rss_version' );
  add_filter( 'wp_head', 'bones_remove_wp_widget_recent_comments_style', 1 );
  add_action( 'wp_head', 'bones_remove_recent_comments_style', 1 );
  add_filter( 'gallery_style', 'bones_gallery_style' );
	add_filter( 'the_content', 'bones_filter_ptags_on_images' );
  add_filter( 'excerpt_more', 'bones_excerpt_more' );
  add_action( 'wp_enqueue_scripts', 'bones_scripts_and_styles', 999 ); // enqueue base scripts and styles
  bones_theme_support();
  //add_action( 'widgets_init', 'bones_register_sidebars' );

}
add_action( 'after_setup_theme', 'bones_ahoy' );


if ( ! isset( $content_width ) ) { $content_width = 1400; } //OEMBED SIZE OPTIONS

// Thumbnail sizes
add_image_size( 'full-page', 4000, 3000, false );
add_image_size( 'large-retina', 2500, 1000, false );
add_image_size( 'medium-retina', 700, 500, false );
add_image_size( 'slider-casa', 970, 570, true );
add_image_size( 'slider-thumb', 300, 300, true );
add_image_size( 'slider-news', 770, 385, true );

add_filter( 'image_size_names_choose', 'bones_custom_image_sizes' );
function bones_custom_image_sizes( $sizes ) {
    return array_merge( $sizes, array(
        'large-retina' => __('Large Retina'),
				'slider-thumb' => __('Thumbnail Slider'),
    ) );
}

/************* THEME CUSTOMIZE *********************/
function custom_theme_customizer($wp_customize) {
  // $wp_customize calls go here.
  //
  // Uncomment the below lines to remove the default customize sections 

  // $wp_customize->remove_section('title_tagline');
  // $wp_customize->remove_section('colors');
  // $wp_customize->remove_section('background_image');
  // $wp_customize->remove_section('static_front_page');
  // $wp_customize->remove_section('nav');

  // Uncomment the below lines to remove the default controls
  // $wp_customize->remove_control('blogdescription');
  
  // Uncomment the following to change the default section titles
  // $wp_customize->get_section('colors')->title = __( 'Theme Colors' );
  // $wp_customize->get_section('background_image')->title = __( 'Images' );
}

add_action( 'customize_register', 'custom_theme_customizer' );

/************** MENUS ***********************/
register_nav_menus(
	array(
		'header-nav' => __( 'Header Menu', 'customtheme' ),
		'footer-nav' => __( 'Footer Menu', 'customtheme' ),
	)
);

/************* ACTIVE SIDEBARS ********************/

// Sidebars & Widgetizes Areas
/*function bones_register_sidebars() {
	register_sidebar(array(
		'id' => 'sidebar1',
		'name' => __( 'Sidebar 1', 'bonestheme' ),
		'description' => __( 'The first (primary) sidebar.', 'bonestheme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h4 class="widgettitle">',
		'after_title' => '</h4>',
	));
} */

/* DON'T DELETE THIS CLOSING TAG */ ?>
<?php include_once('functions-custom.php'); ?>
<?php// include_once('functions-woocommerce.php'); ?>

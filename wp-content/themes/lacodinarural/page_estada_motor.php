<?php
/*Template Name: Estada & motor*/
get_header(); ?>

<main>
	<div id="sliderHomeWrap" class="proportion" data-numerador="5" data-denominador="2" data-maxheight="true">
		<span id="titular_slider" class="sans vertical-align scrollSensible"><?php the_field('titular_slider'); ?></span>
		<div id="sliderHome">
			<?php 
			$images = get_field('slider');
			if( $images ): foreach( $images as $image ): ?>
				<?/*<img src="<?php echo $image['sizes']['slider-casa']; ?>" alt="<?php echo $image['alt']; ?>" />*/?>
				<div style="width: 100%; height: 100%; background-image: url('<?php echo $image['sizes']['large-retina'] ?>'); background-size: cover; background-position: center;"></div>
			<?php endforeach; endif; ?>
		</div>
	</div>
	<div class="container">
		<div class="content-main">
			<div id="content" class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12 scrollSensible"><?php the_field('content'); ?></div>
			<?php checkered('left'); ?>
		</div>
	</div>
</main>

<?php get_footer(); ?>

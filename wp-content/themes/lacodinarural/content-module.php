<?php $sectionID = get_the_ID(); ?>
<section id="<?php echo get_post_field( 'post_name', get_post() ); ?>">
	<div class="module-heading scrollSensible comeFromTop col-lg-12 col-md-12 col-sm-12 col-xs-12">
		<h2><?php the_title(); ?></h2>
		<?php if(get_field('subtitol')){ echo "<span class='subtitle'>".get_field('subtitol')."</span>"; } ?>
	</div>
	<?php $image = get_field('big_image');
	if( !empty($image) ): $url = $image['url']; $alt = $image['alt']; $size = 'large-retina'; $thumb = $image['sizes'][ $size ]; ?>
		<?/*<img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>" />*/?>
		<div class="moduleImage proportion" data-numerador="5" data-denominador="3" data-parallax="scroll" data-image-src="<?= $thumb; ?>" data-speed="0.85" style="float: left;"></div>
	<?php endif; ?>
	<div class="row"><div class="content scrollSensible comeFromTop col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12"><?php the_field('content'); ?></div></div>
	<?php if( have_rows('slider') ): $countThumbs = 0; ?>
	<div class="slider-nav row">
		<?php while ( have_rows('slider') ) : the_row();
			$titol = get_sub_field('titol');
			$imatge = get_sub_field('imatge');
			$imatgeThumb = $imatge['sizes']['slider-thumb'];
			$text_miniatura = get_sub_field('text_featured');
			$isFeatured = get_sub_field('is-featured-image');
		if($isFeatured == true){ ?>
		<div class="slider-thumb display-inline-block col-lg-3 col-md-3 col-sm-6 col-xs-12" data-toggle="modal" data-target="#slider-modal-<?= $sectionID; ?>" data-slide="<?= $countThumbs; ?>">
			<div class="thumb-wrap scrollSensible" style="transition-delay: <?= 0.1*$countThumbs; ?>s; -webkit-transition-delay: <?= 0.3*$countThumbs+0.1; ?>s;">
				<div class="blind"></div>
				<img src="<?php echo $imatgeThumb; ?>">
			</div>
			<span class="thumb-caption"><?php echo $text_miniatura; ?></span>
		</div>
		<?php $countThumbs++; } endwhile; ?>
	</div>
	<?php endif; ?>
	<?php if( have_rows('slider') ): ?>
	<div id="slider-modal-<?= $sectionID; ?>" class="slider-modal modal" tabindex="-1" role="dialog" aria-labelledby="section<?= $sectionID; ?>Modal">
		<img class="modal-close" src="<?php bloginfo('template_url'); ?>/library/images/plus.svg" data-dismiss="modal" aria-label="Close">
		<div class="slider row">
			<?php while ( have_rows('slider') ) : the_row();
				$titol = get_sub_field('titol');
				$imatge = get_sub_field('imatge');
				$imatgeThumb = $imatge['sizes']['full-page'];
				$text_miniatura = get_sub_field('text_featured');
				$text_slider = get_sub_field('text_slider');
			?>
			<div class="slider-slide">
				<div class="slider-img" style="background-image: url('<?php echo $imatgeThumb; ?>');"></div>
				<div class="slider-caption">
					<span>
						<h3><?php echo $titol; ?></h3>
						<?php if($text_slider){ echo $text_slider; } else { echo $text_miniatura; } ?>
					</span>
				</div>
			</div>
			<?php endwhile; ?>
		</div>
	</div>
	<?php endif; ?>
</section>

   


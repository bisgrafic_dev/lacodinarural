<?php
/*Template Name: Notícies*/
get_header(); ?>

<main>
	<h1><?php the_title(); ?></h1>
	<div id="noticies_list" class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
		<?php
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args = array(
			'post_type'=>'post',
			'posts_per_page'=>3,
			'orderby' => 'date',
			'order' => 'DESC',
			'paged' => $paged
		);
		$query = new WP_Query($args);
		if($query->have_posts()): while($query->have_posts()): $query->the_post(); ?>
		<div class="noticia scrollSensible comeFromTop">
			<span class="date sans"><?= get_the_date('j - n - Y'); ?></span>
			<h2><?php the_title();?></h2>
			<?php $images = get_field('slider'); if( $images ): ?>
				<div class="post-slider">
					<?php foreach( $images as $image ): ?>
						<img src="<?php echo $image['sizes']['slider-news']; ?>" alt="<?php echo $image['alt']; ?>" />
					<?php endforeach; ?>
				</div>
			<?php endif;
			$content = get_field('content');
			$contentLength = str_word_count($content);
			if($contentLength <= 45){ //Less than 45 words - no read more ?>
				<div class="post-content">
					<?php echo $content; ?>
				</div>
			<?php }
			else { //More than 45 words - excerpt and read more  ?>
				<div class="post-content">
					<?php echo implode(' ', array_slice(explode(' ', $content), 0, 45)).'...'; ?>
				</div>
				<a class="read-more" href="<?php the_permalink();?>"><?= __('Llegir més');?></a>
			<?php } ?>
		</div>
		
		<?php endwhile; endif;
		wp_reset_postdata();
		?>
        <div class="navigation">
			<?php $big = 999999999; // need an unlikely integer
			echo paginate_links( array(
				'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
				'format' => '/page/%#%',
				'current' => max( 1, get_query_var('paged') ),
				'total' =>$query->max_num_pages,
				'mid_size' => 3,
				'prev_next' => false,
				//'prev_text' => __(''),
				//'next_text' => 
			) ); ?>
        </div>
	</div>
</main>

<?php get_footer(); ?>

<?php
/*Template Name: Home*/
get_header(); ?>

<main>
	<?php
	$laCodina = get_field('la-codina');
	$laCasa = get_field('la-casa');
	$circuit = get_field('circuit');
	$gastronomia = get_field('gastronomia');
	$xeviPons = get_field('xevi-pons'); ?>
	<div class="home-row sans">
		<?php if($laCodina){ foreach ($laCodina as $laCodinaField){ $laCodinaImg = $laCodinaField['img']; ?>
		<a href="<?php pageAnchor($laCodinaField['link']); ?>">
			<div class="home-block home-block-full home-block-responsive-in home65">
				<div class="home-block-background" style="background-image: url('<?php echo $laCodinaImg['sizes']['large-retina']; ?>');"></div>
				<div class="home-caption">
					<b><?php echo $laCodinaField['title']; ?></b>
					<?php echo $laCodinaField['text']; ?>
				</div>
			</div>
		</a>
		<?php }} ?>
		<div class="home-block-container home-block-full home-block-responsive-out home35">
			<?php if($laCasa){ foreach ($laCasa as $laCasaField){ $laCasaImg = $laCasaField['img']; ?>
			<a href="<?php pageAnchor($laCasaField['link']); ?>">
				<div class="home-block">
				<div class="home-block-background" style="background-image: url('<?php echo $laCasaImg['sizes']['large']; ?>');"></div>
					<div class="home-caption">
						<b><?php echo $laCasaField['title']; ?></b>
						<?php echo $laCasaField['text']; ?>
					</div>
				</div>
			</a>
			<?php }} ?>
			<?php if($circuit){ foreach ($circuit as $circuitField){ $circuitImg = $circuitField['img']; ?>
			<a href="<?php pageAnchor($circuitField['link']); ?>">
				<div class="home-block home-block-responsive-out">
				<div class="home-block-background" style="background-image: url('<?php echo $circuitImg['sizes']['large']; ?>');"></div>
					<div class="home-caption">
						<b><?php echo $circuitField['title']; ?></b>
						<?php echo $circuitField['text']; ?>
					</div>
				</div>
			</a>
			<?php }} ?>
		</div>
	</div>
	<div class="home-row sans home-block-responsive-out proportion" data-numerador="5" data-denominador="1.2">
		<?php if($gastronomia){ foreach ($gastronomia as $gastronomiaField){ $gastronomiaImg = $gastronomiaField['img']; ?>
		<a href="<?php pageAnchor($gastronomiaField['link']); ?>">
			<div class="home-block home35">
			<div class="home-block-background" style="background-image: url('<?php echo $gastronomiaImg['sizes']['large']; ?>');"></div>
				<div class="home-caption">
					<b><?php echo $gastronomiaField['title']; ?></b>
					<?php echo $gastronomiaField['text']; ?>
				</div>
			</div>	
		</a>
		<?php }} ?>
		<?php if($xeviPons){ foreach ($xeviPons as $xeviPonsField){ $xeviPonsImg = $xeviPonsField['img']; ?>
		<a href="<?php pageAnchor($xeviPonsField['link']); ?>">
			<div class="home-block home65">
				<div class="home-block-background" style="background-image: url('<?php echo $xeviPonsImg['sizes']['large']; ?>');"></div>
				<div class="home-caption">
					<b><?php echo $xeviPonsField['title']; ?></b>
					<?php echo $xeviPonsField['text']; ?>
				</div>
			</div>
		</a>
		<?php }} ?>
	</div>
	<div id="down"><img src="<?php bloginfo('template_url'); ?>/library/images/arrow-long-45.svg"></div>
	<div id="responsive-slider">
		<?php
		if($laCodina){ ?><div style="background-image: url('<?php echo $laCodinaImg['sizes']['large-retina']; ?>');"><span class="sans vertical-align"><?php echo $laCodinaField['title']; ?></span></div><?php }
		if($laCasa){ ?><div style="background-image: url('<?php echo $laCasaImg['sizes']['large-retina']; ?>');"><span class="sans vertical-align"><?php echo $laCasaField['title']; ?></span></div><?php }
		if($circuit){ ?><div style="background-image: url('<?php echo $circuitImg['sizes']['large-retina']; ?>');"><span class="sans vertical-align"><?php echo $circuitField['title']; ?></span></div><?php }
		if($gastronomia){ ?><div style="background-image: url('<?php echo $gastronomiaImg['sizes']['large-retina']; ?>');"><span class="sans vertical-align"><?php echo $gastronomiaField['title']; ?></span></div><?php }
		if($xeviPons){ ?><div style="background-image: url('<?php echo $xeviPonsImg['sizes']['large-retina']; ?>');"><span class="sans vertical-align"><?php echo $xeviPonsField['title']; ?></span></div><?php } ?>
	</div>
</main>

<?php get_footer(); ?>

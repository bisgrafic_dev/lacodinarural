<?php
/*Template Name: Contactar*/
get_header(); ?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript">
(function($) {
function new_map( $el ) {
	var $markers = $el.find('.marker');
	var args = {
		zoom		: 16,
		center		: new google.maps.LatLng(0, 0),
		mapTypeId	: google.maps.MapTypeId.SATELLITE,
		scrollwheel: false,
		mapTypeControl: false,
		
	};	        	
	var map = new google.maps.Map( $el[0], args);
	map.markers = [];
	$markers.each(function(){
    	add_marker( $(this), map );
	});
	center_map( map );
	return map;
}
function add_marker( $marker, map ) {
	var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );
	var marker = new google.maps.Marker({
		position	: latlng,
		map			: map,
		icon: document.location.origin+"/wp-content/themes/lacodinarural/library/images/marker.png"
	});
	map.markers.push( marker );
}

function center_map( map ) {
	var bounds = new google.maps.LatLngBounds();
	$.each( map.markers, function( i, marker ){
		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );
		bounds.extend( latlng );
	});
	if( map.markers.length == 1 )
	{
	    map.setCenter( bounds.getCenter() );
	    map.setZoom( 16 );
	}
	else
	{
		map.fitBounds( bounds );
	}
}
var map = null;

$(document).ready(function(){
	$('.acf-map').each(function(){
		map = new_map( $(this) );
	});
});
})(jQuery);
</script>
<main>
	<div class="container">
		<h1><?php the_title(); ?></h1>
		<?php //menuChildrenPages(); ?>
	</div>
	<div id="mapa">
		<?php $location = get_field('mapa'); if( !empty($location) ): ?>
		<div class="acf-map proportion" data-numerador="5" data-denominador="2" data-maxHeight="true">
			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
		</div>
		<?php endif; ?>
	</div>
	<div class="container no-bullet no-borders">
		<?php childrenPages(); ?>
		<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12"><?php echo do_shortcode(get_field('formulari')); ?></div>
	</div>
	
</main>

<?php get_footer(); ?>

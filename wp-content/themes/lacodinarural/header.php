<!doctype html>

<!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->

<head>
	<meta charset="utf-8">
	<?php // force Internet Explorer to use the latest rendering engine available ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php wp_title(''); ?></title>
	<?php // mobile meta (hooray!) ?>
	<meta name="HandheldFriendly" content="True">
	<meta name="MobileOptimized" content="320">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<?php // icons & favicons  ?>
	<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/library/images/icons/apple-touch-icon.png">
	<link rel="icon" href="<?php echo get_template_directory_uri(); ?>/library/images/icons/favicon.png">
	<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/library/images/icons/favicon.ico">
	<![endif]-->
	<?php // or, set /favicon.ico for IE10 win ?>
	<meta name="msapplication-TileColor" content="#f01d4f">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/library/images/icons/win8-tile-icon.png">
	<meta name="theme-color" content="#121212">

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
	
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
	<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<?php wp_head(); ?>
</head>
<?php
require_once 'library/Mobile-Detect/Mobile_Detect.php';
$detect = new Mobile_Detect;
$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
$scriptVersion = $detect->getScriptVersion();
$isTouch = $detect->isMobile();
$browser = getBrowser();
if($isTouch){ $touch = 'isTouch'; } else { 'noTouch'; } ?>
<body <?php body_class($browser['name'].' '.$touch); ?> itemscope itemtype="http://schema.org/WebPage">
	<div id="container">
		<header class="text-align-left" role="banner" itemscope itemtype="http://schema.org/WPHeader">
			<a id="logo" href="<?php bloginfo('url'); ?>">
				<!--img class="scrollSensible" src="<?php bloginfo('template_url'); ?>/library/images/logo.svg"-->
			<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
				 viewBox="0 0 114 80" style="enable-background:new 0 0 114 80;" xml:space="preserve">
			<style type="text/css">
				.st0{fill:#010202;}
			</style>
			<g>
				<polygon class="st0" points="75,11.3 75.2,11.2 75.2,10 75,9.9 69.7,9.9 69.5,9.7 69.5,0.2 69.4,0 68.1,0 67.9,0.2 67.9,11.2 
					68.1,11.3 	"/>
				<path class="st0" d="M80.9,6.5c0,0-0.6-0.2-1.5-0.2c-2.1,0-3.6,0.8-3.6,2.6c0,1.6,1,2.6,2.7,2.6c1.6,0,2.3-1,2.3-1h0.2l0.2,0.6
					l0.2,0.2h1.1l0.2-0.2V6.6c0-2.4-0.8-3.6-3.2-3.6c-2.4,0-3.2,1.3-3.2,2.4l0.2,0.2h1.3l0.2-0.2c0-0.6,0.5-1,1.6-1
					c1.1,0,1.6,0.3,1.6,1.5v0.3L80.9,6.5z M81.1,8.4c0,1-0.8,1.6-2.1,1.6c-1,0-1.5-0.3-1.5-1.1c0-0.8,0.6-1.3,2.1-1.3
					c0.8,0,1.3,0.2,1.3,0.2l0.2,0.2V8.4z"/>
				<path class="st0" d="M72.6,17.1c1.6,0,2.6,0.8,2.9,1.9l0.2,0.2H77l0.2-0.2c-0.3-1.9-1.9-3.4-4.5-3.4c-3.4,0-5.3,2.4-5.3,5.8
					c0,3.4,1.9,5.8,5.3,5.8c2.7,0,4.2-1.6,4.5-3.6L77,23.5h-1.3l-0.2,0.2c-0.3,1.1-1.1,2.1-2.9,2.1c-2.4,0-3.7-1.8-3.7-4.4
					S70.2,17.1,72.6,17.1z"/>
				<path class="st0" d="M82,18.8c-2.4,0-3.9,1.6-3.9,4.2c0,2.6,1.5,4.2,3.9,4.2c2.4,0,3.9-1.6,3.9-4.2C85.9,20.4,84.4,18.8,82,18.8z
					 M82,25.8c-1.5,0-2.3-1-2.3-2.7c0-1.8,0.8-2.7,2.3-2.7c1.5,0,2.3,1,2.3,2.7C84.3,24.8,83.5,25.8,82,25.8z"/>
				<path class="st0" d="M90.3,27.2c1.5,0,2.1-1,2.1-1h0.2l0.2,0.6l0.2,0.2H94l0.2-0.2v-11L94,15.8h-1.3l-0.2,0.2v3.9h-0.2
					c0,0-0.6-1-2.1-1c-2.3,0-3.6,1.8-3.6,4.2C86.7,25.5,88,27.2,90.3,27.2z M90.4,20.3c1.3,0,2.1,0.8,2.1,2.7c0,1.9-0.8,2.7-2.1,2.7
					c-1.3,0-2.1-1-2.1-2.7C88.3,21.3,89.2,20.3,90.4,20.3z"/>
				<polygon class="st0" points="97.3,27.1 97.5,26.9 97.5,19.2 97.3,19 96,19 95.9,19.2 95.9,26.9 96,27.1 	"/>
				<polygon class="st0" points="97.3,17.7 97.5,17.5 97.5,16.2 97.3,16.1 96,16.1 95.9,16.2 95.9,17.5 96,17.7 	"/>
				<path class="st0" d="M103,18.8c-1.5,0-2.1,1-2.1,1h-0.2l-0.2-0.6l-0.2-0.2h-1.1l-0.2,0.2v7.8l0.2,0.2h1.3l0.2-0.2V23
					c0-1.9,0.6-2.7,2.1-2.7c1.5,0,1.6,1.1,1.6,1.9v4.7l0.2,0.2h1.3l0.2-0.2v-4.7C106.1,20.3,105.3,18.8,103,18.8z"/>
				<path class="st0" d="M110.8,18.8c-2.4,0-3.2,1.3-3.2,2.4l0.2,0.2h1.3l0.2-0.2c0-0.6,0.5-1,1.6-1c1.1,0,1.6,0.3,1.6,1.5v0.3
					l-0.2,0.2c0,0-0.6-0.2-1.5-0.2c-2.1,0-3.6,0.8-3.6,2.6c0,1.6,1,2.6,2.7,2.6c1.6,0,2.3-1,2.3-1h0.2l0.2,0.6l0.2,0.2h1.1l0.2-0.2
					v-4.5C114,20,113.2,18.8,110.8,18.8z M112.4,24.2c0,1-0.8,1.6-2.1,1.6c-1,0-1.5-0.3-1.5-1.1s0.6-1.3,2.1-1.3c0.8,0,1.3,0.2,1.3,0.2
					l0.2,0.2V24.2z"/>
				<path class="st0" d="M76.7,34.7c0-2.2-1.4-3.2-3.7-3.2h-4.9l-0.2,0.2v10.8l0.2,0.2h1.3l0.2-0.2v-4.5l0.2-0.2h2.5l0.3,0.2l2.9,4.5
					l0.3,0.2h0.6l0.2-0.2v-1.1l-2.2-3.5v-0.2C75.7,37.4,76.7,36.5,76.7,34.7z M73,36.5h-3.3l-0.2-0.2v-3.2l0.2-0.2H73
					c1.4,0,2.1,0.5,2.1,1.8C75.1,36,74.4,36.5,73,36.5z"/>
				<path class="st0" d="M83.2,34.7L83,34.9v3.8c0,1.9-0.8,2.7-1.9,2.7c-1.3,0-1.6-1-1.6-1.9v-4.6l-0.2-0.2h-1.3l-0.2,0.2v4.6
					c0,1.8,0.8,3.3,3,3.3c1.3,0,1.9-1,1.9-1H83l0.2,0.6l0.2,0.2h1.1l0.2-0.2v-7.6l-0.2-0.2H83.2z"/>
				<path class="st0" d="M90.4,34.7c-1.1,0-1.6,0.3-2.1,0.8h-0.2L88,34.9l-0.2-0.2h-1.1l-0.2,0.2v7.6l0.2,0.2H88l0.2-0.2v-4
					c0-1.9,0.8-2.4,2.1-2.4H91l0.2-0.2v-1.1L91,34.7H90.4z"/>
				<path class="st0" d="M95.2,34.5c-2.4,0-3.2,1.3-3.2,2.4l0.2,0.2h1.3l0.2-0.2c0-0.6,0.5-1,1.6-1c1.1,0,1.6,0.3,1.6,1.4v0.3l-0.2,0.2
					c0,0-0.6-0.2-1.4-0.2c-2.1,0-3.5,0.8-3.5,2.5c0,1.6,1,2.5,2.7,2.5c1.6,0,2.2-1,2.2-1h0.2l0.2,0.6l0.2,0.2h1.1l0.2-0.2v-4.5
					C98.3,35.7,97.5,34.5,95.2,34.5z M96.7,39.8c0,1-0.8,1.6-2.1,1.6c-1,0-1.4-0.3-1.4-1.1s0.6-1.3,2.1-1.3c0.8,0,1.3,0.2,1.3,0.2
					l0.2,0.2V39.8z"/>
				<polygon class="st0" points="100.3,31.5 100.1,31.7 100.1,42.5 100.3,42.7 101.5,42.7 101.7,42.5 101.7,31.7 101.5,31.5 	"/>
			</g>
			<g id="escut" class="scrollSensible">
				<path class="st0" d="M52.8,0H1.7H0v1.7v25.5c0,7.1,1.4,14,4.1,20.5c2.7,6.3,6.5,11.9,11.3,16.8c4.8,4.8,10.5,8.6,16.8,11.3
					c6.5,2.8,13.4,4.1,20.5,4.1h1.7v-1.7V1.7V0H52.8z M52.8,78.3c-28.2,0-51.1-22.9-51.1-51.1V1.7h51.1V78.3z"/>
				<path class="st0" d="M26,27.4h8.8l1.3-1.3l-4.4-7.1c2.1-0.6,3.6-2.5,3.6-4.7c0-2.7-2.2-4.9-4.9-4.9s-4.9,2.2-4.9,4.9
					c0,2.2,1.5,4.1,3.5,4.7l-4.3,7.2L26,27.4z M27.2,14.2c0-1.8,1.5-3.3,3.3-3.3s3.3,1.5,3.3,3.3c0,1.8-1.5,3.3-3.3,3.3
					S27.2,16,27.2,14.2z M30.4,19.9l3.5,5.8h-7L30.4,19.9z"/>
				<path class="st0" d="M47.7,27.2l0.8,0.1v-1.7L48,25.6c-2.8-0.4-5.2-1.9-6.8-4.1c-0.8-1-1.3-2.1-1.6-3.3c0.6,0.5,1.4,0.8,2.2,0.9
					c0.2,0,0.5,0.1,0.7,0.1c2.4,0,4.5-1.8,4.9-4.2c0.2-1.3-0.1-2.6-0.9-3.7c-0.8-1.1-1.9-1.7-3.2-1.9c-0.2,0-0.5-0.1-0.7-0.1
					c-2.4,0-4.5,1.8-4.9,4.2l0,0c-0.5,3.2,0.3,6.4,2.3,9C41.7,25.1,44.5,26.7,47.7,27.2z M39.2,13.7c0.2-1.6,1.6-2.8,3.2-2.8
					c0.2,0,0.3,0,0.5,0c0.9,0.1,1.6,0.6,2.1,1.3c0.5,0.7,0.7,1.6,0.6,2.4c-0.2,1.6-1.6,2.8-3.2,2.8c-0.2,0-0.3,0-0.5,0
					C40.2,17.2,38.9,15.5,39.2,13.7C39.2,13.7,39.2,13.7,39.2,13.7L39.2,13.7C39.2,13.7,39.2,13.7,39.2,13.7z"/>
				<polygon class="st0" points="8.1,24.1 8.1,26.1 7.1,27.2 7.2,27.4 9,27.4 9,27.4 10.8,27.4 10.9,27.2 9.9,26.1 9.9,24.1 9.9,12.9 
					9.9,10.9 10.9,9.8 10.8,9.6 9,9.6 9,9.6 7.2,9.6 7.1,9.8 8.1,10.9 8.1,12.9 	"/>
				<path class="st0" d="M18.2,27.3c3.1,0,5.6-2.5,5.6-5.6c0-1.9-1.6-3.5-3.5-3.5c-1.9,0-3.5,1.6-3.5,3.5c0,1.2,0.8,2.1,1.9,2.4
					l0.4-1.6c-0.3-0.1-0.6-0.4-0.6-0.8c0-1,0.8-1.8,1.8-1.8s1.8,0.8,1.8,1.8c0,2.2-1.8,4-4,4c-2.2,0-4-1.8-4-4c0-2,1.4-3.6,3.3-3.9
					c2.1-0.3,3.6-2.1,3.6-4.2c0-2.4-1.9-4.3-4.3-4.3v1.7c1.4,0,2.6,1.2,2.6,2.6c0,1.3-0.9,2.4-2.2,2.6c-2.7,0.4-4.7,2.8-4.7,5.6
					C12.6,24.8,15.1,27.3,18.2,27.3z"/>
			</g>
			</svg>

			</a>
			<nav id="desktop-menu" class="vertical-align"><?php wp_nav_menu(array( 'container' => false, 'theme_location' => 'header-nav' )); ?></nav>
			<!--button id="toggleFont"><b>Change to: </b></button-->
			<div id="big-lang"><?php do_action('icl_language_selector'); ?></div>
			<div id="social">
				<?php if(get_field('facebook','option')){ echo '<li><a href="'.get_field('facebook','option').'">Facebook</a></li>'; }
				if(get_field('instagram','option')){ echo '<li><a href="'.get_field('instagram','option').'">Instagram</a></li>'; } ?>
			</div>
			<button class="hamburger hamburger--collapse" type="button">
			<span class="hamburger-box">
			  <span class="hamburger-inner"></span>
			</span>
		  </button>
			<nav id="responsive-menu">
				<div class="vertical-align">
					<?php wp_nav_menu(array( 'container' => false, 'theme_location' => 'header-nav' )); ?>
				</div>
				<div id="social-responsive">
					<?php if(get_field('facebook','option')){ echo '<li><a href="'.get_field('facebook','option').'">Facebook</a></li>'; }
					if(get_field('instagram','option')){ echo '<li><a href="'.get_field('instagram','option').'">Instagram</a></li>'; } ?>
				</div>
				<?php do_action('icl_language_selector'); ?>
			</nav>
		</header>

var wH; var wW; var wHnoFooter; var wHnoHeader;
function variables() {
	wH = $(window).height();
	wW = $(window).width();
	wHnoFooter = wH-$('footer').outerHeight();
	wHnoHeader = wH - 65;
}
/*If element is visible----------------------------------*/
var positionLeft = $.fn.visible = function (partial) {
	var $t = $(this),
			$w = $(window),
			viewTop = $w.scrollTop(),
			viewBottom = viewTop + $w.height(),
			_top = $t.offset().top,
			_bottom = _top + $t.height(),
			compareTop = partial === true ? _bottom : _top,
			compareBottom = partial === true ? _top : _bottom;
	return ((compareBottom <= viewBottom) && (compareTop >= viewTop));
};
function sizeMain(){
	$('main').css('min-height',wHnoFooter);
}
function initSlider() {
	$('.slider').slick({
		infinite: false,
		slidesToShow: 1,
		slidesToScroll: 1,
		prevArrow: '<div class="prev-arrow"><</div>',
		nextArrow: '<div class="prev-arrow">></div>',
		dots: true,
		dotsClass: 'custom_paging',
		customPaging: function (slider, i) {
			return  (i + 1) + '/' + slider.slideCount;
		}
	});
}
function modalSlider(){
	var theModal;
	$('.slider-thumb').click(function(){
		theModal = $(this).data('target');
		theSlide = $(this).data('slide');
		$(theModal).on('shown.bs.modal', function (e) {
			$(this).find('.slider').slick({
				infinite: false,
				slidesToShow: 1,
				slidesToScroll: 1,
				prevArrow: '<div class="slider-arrow prev-arrow vertical-align"></div>',
				nextArrow: '<div class="slider-arrow next-arrow vertical-align"></div>',
				initialSlide: theSlide,
				dots: true,
				dotsClass: 'custom_paging',
				customPaging: function (slider, i) {
					return  (i + 1) + '/' + slider.slideCount;
				}
			});
		});
	});
	$('.modal-close').click(function(){
		$('.slider').slick('unslick');
	});
}
function postSlider(){
	if($('body').hasClass('page-template-page_noticies') || $('body').hasClass('single')){
		$('.post-slider').slick({
					infinite: false,
					slidesToShow: 1,
					slidesToScroll: 1,
					prevArrow: '<div class="slider-arrow prev-arrow vertical-align"></div>',
					nextArrow: '<div class="slider-arrow next-arrow vertical-align"></div>',
					dots: true,
					dotsClass: 'custom_paging',
					customPaging: function (slider, i) {
						return  (i + 1) + '/' + slider.slideCount;
					}
				});
		console.log('heyheyhey');
	}
}
function blocksHome(){
	if($('body').hasClass('home')){
		if($('#responsive-slider').hasClass('slick-initialized')){ $('#responsive-slider').slick('unslick'); }
		if(wW > 768){
			$('.home-block-full').height(wH-65);
		} else {
			$('#responsive-slider').height(wHnoHeader).slick({
														arrows: false,
														infinite: true,
														autoplay: true,
														autoplaySpeed: 2500,
														fade: true,
														draggable: true,
														dots: true,
														dotsClass: 'custom_paging',
														customPaging: function (slider, i) {
															return  (i + 1) + '/' + slider.slideCount;
														}
			});
		}
	}
}
function sliderCasa(){
	if($('body').hasClass('page-template-page_estada_motor')){
		$('#sliderHome').slick({
			autoplay: true,
			autoplaySpeed: 2500,
			fade: true,
			slidesToShow: 1,
			arrows: false,
			infinite: false,
			dots: true,
			dotsClass: 'custom_paging',
			customPaging: function (slider, i) {
				return  (i + 1) + '/' + slider.slideCount;
			}
		});
	}
}
function animateAnchor(){
	$('.anchor-link').click(function(e){
		e.preventDefault();
		var where = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(where).offset().top
		}, 700);
	});
}
function goDown(){
	if($('body').hasClass('home')){
		$('#down').click(function(){
			$('html, body').animate({
				scrollTop: $('footer').offset().top
			}, 700);
			$(this).addClass('gone').fadeOut();
		});
		if(!$('#down').hasClass('gone')){
			$(window).scroll(function(){
				$('#down').addClass('gone').fadeOut();
			});
		}
	}
}
function goTop(){
	$('#goTop').click(function(){
		$('html, body').animate({
			scrollTop: 0
		}, 700);
	});
	
}
function reservesForm(){
	$('#diamonds-reserves .diamond').click(function(){
		theindex = $(this).index();
		$('.diamond-selected').removeClass('diamond-selected');
		$(this).addClass('diamond-selected');
		setTimeout(function(){ $('#reserves form').fadeIn(400); },300);
		$('#reserva .wpcf7-list-item input').prop('checked',false);
		$('#tarifes .tarifa').removeClass('tarifa-visible');
		setTimeout(function(){ $('#tarifes .tarifa').eq(theindex).addClass('tarifa-visible'); },300);
		$('#reserva .wpcf7-list-item').eq(theindex).find('input').prop('checked',true);
	});
}

function positionDiamonds(){
	margin = (wW - $('.container').outerWidth())/4 - 35;
	$('#diamonds_left').css('margin-left',-margin);
	$('#diamonds_right').css('margin-right',-margin);
}

function fixDiamonds(){
	if($('#diamonds')[0]){
		bodyS = $('body').scrollTop();
		diamS = $('#diamonds').offset().top;
		if(bodyS >= diamS/2 && !$('#diamonds').hasClass('fixed')){
			$('#diamonds').addClass('fixed').css('top',diamS-bodyS);
			//console.log('ADD // Body: '+bodyS+' Diamonds: '+diamS-bodyS);
		} else if(bodyS < diamS/2 && $('#diamonds').hasClass('fixed')){
			$('#diamonds').removeClass('fixed').css('top',0);
			//console.log('REMOVE // Body: '+bodyS+' Diamonds: '+diamS-bodyS);
		}
	}
}
function responsiveMenu(){
	$('.hamburger').click(function(){
		$(this).toggleClass('is-active');
		$('#responsive-menu').toggleClass('out');
	});
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) === 0) {
            return c.substring(name.length,c.length);
        }
    }
    return "";
}
function checkCookie() {
    var cookie=getCookie("cookiePolicy");
    if (cookie!=="accepted") {
		$('#cookie-notice').show();
		setTimeout(function(){ $('#cookie-notice').addClass('out'); },200);
    }
	$('#close-cookie').click(function(){
		setCookie("cookiePolicy", "accepted", 365);
		$('#cookie-notice').removeClass('out');
		setTimeout(function(){ $('#cookie-notice').hide(); },200);
	});
}

function proportion(){	
	$('.proportion').each(function(){
		numerador = $(this).data('numerador');
		denominador = $(this).data('denominador');
		maxH = $(this).data('maxheight');
		maxHeight = wH - $(this).offset().top - 80;
		thisWidth = $(this).width();
		multiply = numerador/denominador;
		thisHeight = thisWidth/multiply;
		if(thisHeight > maxHeight && maxH === true){ $(this).height(maxHeight); } else { $(this).height(thisHeight); }
	});
}

function playVideo(){
	$('#play').click(function(){
		$(this).css('z-index',-1);
		$('#video_cover').css('z-index',-1);
		$('#myvideo').play();
	});
}

function changeFont(){
	$('#toggleFont').click(function(){
		$('body').toggleClass('arimo');
		$(this).toggleClass('source');
	});
}

function visibleScroll(){
	$('.scrollSensible, .parallax-mirror').each(function (i, el) {
		el = $(el);
		if (el.visible(true)) {
			el.addClass('visible');
		} else {
			if(el.hasClass('visible')){ el.removeClass('visible'); }
		}
	});
}

function langScroll(){
	wST = $(window).scrollTop();
	if(wST > 0){ $('#big-lang #lang_sel_list').addClass('out'); }
	else { $('#big-lang #lang_sel_list').removeClass('out'); }
}



$body = $('body');
$window = $(window);


//VIDEO---------------------------------------------------------------------------------------------------------------
$body = $('body');
$window = $(window);


/*********
 Froogaloop
 **********/
var Froogaloop = function () {
    function e(a) {
        return new e.fn.init(a)
    }
    function g(a, c, b) {
        if (!b.contentWindow.postMessage)
            return!1;
        a = JSON.stringify({method: a, value: c});
        b.contentWindow.postMessage(a, h)
    }
    function l(a) {
        var c, b;
        try {
            c = JSON.parse(a.data), b = c.event || c.method
        } catch (e) {
        }
        "ready" != b || k || (k = !0);
        if (!/^https?:\/\/player.vimeo.com/.test(a.origin))
            return!1;
        "*" === h && (h = a.origin);
        a = c.value;
        var m = c.data, f = "" === f ? null : c.player_id;
        c = f ? d[f][b] : d[b];
        b = [];
        if (!c)
            return!1;
        void 0 !== a && b.push(a);
        m && b.push(m);
        f && b.push(f);
        return 0 < b.length ? c.apply(null, b) : c.call()
    }
    function n(a, c, b) {
        b ? (d[b] || (d[b] = {}), d[b][a] = c) : d[a] = c
    }
    var d = {}, k = !1, h = "*";
    e.fn = e.prototype = {element: null, init: function (a) {
            "string" === typeof a && (a = document.getElementById(a));
            this.element = a;
            return this
        }, api: function (a, c) {
            if (!this.element || !a)
                return!1;
            var b = this.element, d = "" !== b.id ? b.id : null, e = c && c.constructor && c.call && c.apply ? null : c, f = c && c.constructor && c.call && c.apply ? c : null;
            f && n(a, f, d);
            g(a, e, b);
            return this
        }, addEvent: function (a, c) {
            if (!this.element)
                return!1;
            var b = this.element, d = "" !== b.id ? b.id : null;
            n(a, c, d);
            "ready" != a ? g("addEventListener", a, b) : "ready" == a && k && c.call(null, d);
            return this
        }, removeEvent: function (a) {
            if (!this.element)
                return!1;
            var c = this.element, b = "" !== c.id ? c.id : null;
            a:{
                if (b && d[b]) {
                    if (!d[b][a]) {
                        b = !1;
                        break a
                    }
                    d[b][a] = null
                } else {
                    if (!d[a]) {
                        b = !1;
                        break a
                    }
                    d[a] = null
                }
                b = !0
            }
            "ready" != a && b && g("removeEventListener", a, c)
        }};
    e.fn.init.prototype = e.fn;
    window.addEventListener ? window.addEventListener("message", l, !1) : window.attachEvent("onmessage", l);
    return window.Froogaloop =
            window.$f = e
}();



/*********
 Vimeo 
 **********/
vimeo = {
    init: function () {
        //if (TOUCH) return;
        $('.vimeoWrapper').each(function () {
            $(this).find('.play_vimeo').click(function () {
                vimeo.play($(this));
            });
        });
    },
    play: function ($play_button) {
        //if (TOUCH) return;
        vimeo.pauseAll();
        youtube.onPlay();
        $play_button.addClass('isPlaying').fadeOut();
        var id = $play_button.data('videoid');
        var iframe = document.getElementById(id);
        var player = $f(iframe);
        player.api("play");
    },
    pause: function ($play_button) {
        //if (TOUCH) return;
        $play_button.removeClass('isPlaying').fadeIn();
        var id = $play_button.data('videoid');
        var iframe = document.getElementById(id);
        var player = $f(iframe);
        player.api("pause");
    },
    pauseAll: function () {
        //if (TOUCH) return;
        $('.vimeoWrapper .play_vimeo.isPlaying').each(function () {
            vimeo.pause($(this));
        });

    }
}
$(function () {
    vimeo.init();
});

/*********
 Youtube 
 **********/


youtube = {
    init: function () {
        youtube.injectAPI();
        youtube.player = [];
    },
    onYouTubePlayerAPIReady: function () {
        var i = 0;
        $('.youtubePlayer').each(function () {
			console.log('there is a player');
            var $this = $(this);
            var videoId = 'videoAPI' + i;
            $this.attr('id', videoId);
            var $parent = $this.parent('.media-wrapper');
            var $button = $parent.find('.play_youtube');
            youtube.player[i] = new YT.Player(videoId, {
                playerVars: {
                    conotrols: 0
                },
                events: {
                    'onReady': youtube.onPlayerReady(i, $this, $button),
                    'onStateChange': youtube.onPlayerChange
                }
            });
            i = i + 1;
        });
    },
    onPlayerChange: function (event) {
        if (event.data === 2) {
            youtube.onPause(event.target);
        }
        if (event.data === 1) {
            youtube.onPlay(event.target);
        }
    },
    onPause: function (target) {

    },
    onPlay: function (target) {
        if (typeof target === 'undefined') {
            target = false;
        }
        for (var i = 0; i < youtube.player.length; i++) {

            if (youtube.player[i] !== target || target === false) {
                var $iframe = $(youtube.player[i].c);
                var $parent = $iframe.parents('.media-wrapper');
                var $playCover = $parent.find('.play_youtube');
                youtube.player[i].pauseVideo();
                console.log(youtube.player[i]);
                $playCover.fadeIn();
            }
        }
        vimeo.pauseAll();
    },
    onPlayerReady: function (i, video, $button) {
		console.log('player ready');
        $button.click(function () {
            $button.fadeOut();
            video.fadeIn(function () {
                youtube.player[i].playVideo();
            });

        });
    },
    pause: function () {

    },
    injectAPI: function () {
        var tag = document.createElement('script');
        tag.src = "//www.youtube.com/player_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    }
};
$('.play_youtube').click(function(){
	console.log($(this).attr('id')+', '+$(this).attr('class'));
});

//END VIDEO---------------------------------------------------------------------------------------------------------------

$(function() {
	modalSlider();
	sliderCasa();
	goDown();
	goTop();
	animateAnchor();
	reservesForm();
	responsiveMenu();
	postSlider();onResize();
});
$(window).scroll(function() {
   fixDiamonds();
   visibleScroll();
   langScroll();
});
function onResize() {
	variables();
	blocksHome();
	sizeMain();
	proportion();
	positionDiamonds();
}
$(window).load(function() {
	$('main').css('opacity',1);
	checkCookie();
	visibleScroll();
});
$(window).resize(function() {
  onResize();
});

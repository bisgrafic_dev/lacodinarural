			<footer class="sans" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
				<div class="container-fluid">
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<strong><?php the_field('footer-title', 'option'); ?></strong>&nbsp;&nbsp;&nbsp;<?php the_field('footer-col1', 'option'); ?>
					</div>
					<nav class="col-lg-3 col-md-3 col-sm-12 col-xs-12"><?php wp_nav_menu(array( 'container' => false, 'theme_location' => 'footer-nav' )); ?></nav>
				</div>
			</footer>
			<div id="cookie-notice">
				<div class="col-lg-6 col-md-6 cols-sm-8 col-xs-10 sans">
					<img src="<?php bloginfo('template_url'); ?>/library/images/cookies.svg">En aquest web utilitzem cookies pròpies i de tercers per millorar els nostres serveis i mostrar informació relacionada amb les seves preferències mitjançant l'anàlisi dels teus hàbits de navegació. Si continua navegant, considerem que accepta el seu ús.  Podeu canviar la configuració o obtenir més informacióaquí.</span>
				</div>
				<div id="close-cookie"><img src="<?php bloginfo('template_url'); ?>/library/images/close.svg"></div>
			</div>
		</div>
		<?php wp_footer(); ?>
		<script class="">
			youtube.init();
			function onYouTubePlayerAPIReady() {
				youtube.onYouTubePlayerAPIReady();
			}
		</script>
	</body>
</html>

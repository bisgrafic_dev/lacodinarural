<?php
/*Template Name: Reserva*/
get_header();
if(have_posts()): while(have_posts()): the_post() ?>
<main id="reserves">
	<div class="container no-bullet">
		<h1><?php the_title(); ?></h1>
		<div id="content" class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
		<?php the_content(); ?>
		</div>
		<div id="diamonds-reserves" class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12">
			<span class="diamond scrollSensible comeFromTop">
				<span class="diamond-text vertical-align"><?= __('Circuit','lacodinarural'); ?></span>
				<span class="diamond-background"></span>
			</span>
			<span class="diamond scrollSensible comeFromTop" style="transition-delay: 0.3s; -webkit-transition-delay: 0.3s;">
				<span class="diamond-text vertical-align"><?= __('Casa','lacodinarural'); ?></span>
				<span class="diamond-background"></span>
			</span>
			<span class="diamond scrollSensible comeFromTop" style="transition-delay: 0.6s; -webkit-transition-delay: 0.6s;">
				<span class="diamond-text vertical-align"><?= __('Casa + circuit','lacodinarural'); ?></span>
				<span class="diamond-background"></span>
			</span>
		</div>
		<div id="tarifes">
			<div class="tarifa">
			<?php if( have_rows('temporada-alta-circuit-repeater','option') && have_rows('temporada-baixa-circuit-repeater','option')){ ?>
				<div class="temporada sans col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<span class="title-tarifa"><?php the_field('temporada-alta-circuit','option'); ?></span>
					<div class="tarifes-table">
					<?php if( have_rows('temporada-alta-circuit-repeater','option') ): while ( have_rows('temporada-alta-circuit-repeater','option') ) : the_row(); echo '<span class="table-tarifes-row">';
							echo '<span>'.get_sub_field('label').' </span>';
							echo '<span>'.get_sub_field('price').' €</span>';
					echo '</span>'; endwhile; endif; ?>
					</div>
				</div>
				<div class="temporada sans col-lg-6 col-md-6 col-sm-6 col-xs-6">
					<span class="title-tarifa"><?php the_field('temporada-baixa-circuit','option'); ?></span>
					<div class="tarifes-table">
					<?php if( have_rows('temporada-baixa-circuit-repeater','option') ): while ( have_rows('temporada-baixa-circuit-repeater','option') ) : the_row(); echo '<span class="table-tarifes-row">';
							echo '<span>'.get_sub_field('label').' </span>';
							echo '<span>'.get_sub_field('price').' €</span>';
					echo '</span>'; endwhile; endif; ?>	
					</div>
				</div>
			<?php } ?>
			</div>
			<div class="tarifa">
			<?php if(have_rows('temporada-alta-casa-repeater','option') && have_rows('temporada-baixa-casa-repeater','option')) { ?>
			
				<div class="temporada sans col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<span class="title-tarifa"><?php the_field('temporada-alta-casa','option'); ?></span>
					<div class="tarifes-table">
					<?php if( have_rows('temporada-alta-casa-repeater','option') ): while ( have_rows('temporada-alta-casa-repeater','option') ) : the_row(); echo '<span class="table-tarifes-row">';
							echo '<span>'.get_sub_field('label').' </span>';
							echo '<span>'.get_sub_field('price').' €</span>';
					echo '</span>'; endwhile; endif; ?>
					</div>
				</div>
				<div class="temporada sans col-lg-6 col-md-6 col-sm-6 col-xs-12">
					<span class="title-tarifa"><?php the_field('temporada-baixa-casa','option'); ?></span>
					<div class="tarifes-table">
					<?php if( have_rows('temporada-baixa-casa-repeater','option') ): while ( have_rows('temporada-baixa-casa-repeater','option') ) : the_row(); echo '<span class="table-tarifes-row">';
							echo '<span>'.get_sub_field('label').' </span>';
							echo '<span>'.get_sub_field('price').' €</span>';
					echo '</span>'; endwhile; endif; ?>
					</div>
				</div>
			<?php } ?>
			<small id="lletraPetita" class="col-lg-12 col-md-12 col-sm-12 col-xs-12"><?php the_field('condicions','option'); ?></small>
			</div>
		</div>
		<?php echo do_shortcode(get_field('formulari_reserva')); ?>
	</div>
</main>

<?php
endwhile; endif;
get_footer(); ?>

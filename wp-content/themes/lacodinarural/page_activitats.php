<?php
/*Template Name: Activitats*/
get_header(); ?>

<main class="container">
	<h1><?php the_title(); ?></h1>
	<?php //menuChildrenPages(); ?>
	<?php childrenPages(); ?>
</main>

<?php get_footer(); ?>

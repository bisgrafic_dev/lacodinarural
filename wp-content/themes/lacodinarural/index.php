<?php get_header();
if(have_posts()): while(have_posts()): the_post() ?>

<main>
	<h1><?php the_title(); ?></h1>
	<div class="col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-10 col-sm-offset-1 col-xs-12"><?php the_content(); ?></div>
</main>

<?php
endwhile; endif;
get_footer(); ?>

<?php
function custom_fonts() {
  wp_enqueue_style('SourceSans', '//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,300italic,400italic,700,700italic');
  wp_enqueue_style('SortsMill', '//fonts.googleapis.com/css?family=Sorts+Mill+Goudy:400,400italic&subset=latin,latin-ext');
	wp_enqueue_style('Arimo','//fonts.googleapis.com/css?family=Arimo:400,400i,700,700i');
}
add_action('wp_enqueue_scripts', 'custom_fonts');

//Rename Posts-------------------------------------------
function revcon_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Notícies';
    $submenu['edit.php'][5][0] = 'Notícies';
    $submenu['edit.php'][10][0] = 'Afegir Notícia';
    echo '';
}
function revcon_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Notícies';
    $labels->singular_name = 'Notícia';
    $labels->add_new = 'Afegir Notícia';
    $labels->add_new_item = 'Afegir Notícia';
    $labels->edit_item = 'Editar Notícia';
    $labels->new_item = 'Notícies';
    $labels->view_item = 'Veure Notícies';
    $labels->search_items = 'Buscar Notícies';
    $labels->not_found = "No s'han trobat Notícies";
    $labels->not_found_in_trash = "No s'han trobat Notícies";
    $labels->all_items = 'Totes les notícies';
    $labels->menu_name = 'Notícies';
    $labels->name_admin_bar = 'Notícies';
}
add_action( 'admin_menu', 'revcon_change_post_label' );
add_action( 'init', 'revcon_change_post_object' );

//Change excerpt------------------------------------------
/*function bones_excerpt_more($more) {
	global $post;
	// edit here if you like
	return '...  <a class="excerpt-read-more" href="'. get_permalink( $post->ID ) . '" title="'. __( 'Read ', 'bonestheme' ) . esc_attr( get_the_title( $post->ID ) ).'">'. __( 'Read more &raquo;', 'bonestheme' ) .'</a>';
}*/

//Detect Browser---------------------------------------


// detect browser
include_once("library/Browser.php");

function getBrowser() {
		$B = new Browser();
		$version = explode(".", $B->getVersion());
		return array(
				"name" => $B->getBrowser(),
				"complete_version" => $B->getVersion(),
				"version" => $version[0]
		);
}


//Options pages-----------------------------
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> '',
		'menu_title'	=> 'Tarifes',
		'menu_slug' 	=> 'tarifes',
		'capability'	=> 'edit_posts',
		'position' => 50,
		'icon_url' => 'data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz48c3ZnIHZlcnNpb249IjEuMSIgaWQ9IkNhcGFfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiIHZpZXdCb3g9IjAgMCA3NS44IDYwIiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA3NS44IDYwOyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSI+PHBhdGggZD0iTTM3LjksMEMxNi43LDAsMCwxMCwwLDIyLjl2MTQuMkMwLDQ5LjksMTYuNyw2MCwzNy45LDYwczM3LjktMTAsMzcuOS0yMi45VjIyLjlDNzUuOCwxMC4xLDU5LjEsMCwzNy45LDB6IE00LDM3LjF2LTMuOWMxLDEuMiwyLjEsMi4zLDMuMywzLjN2OC42QzUuMiw0Mi43LDQsNDAsNCwzNy4xeiBNNDYuMiw0NS4ydjEwLjJjLTIsMC4zLTQuMiwwLjUtNi4zLDAuNlY0NS43QzQyLjEsNDUuNiw0NC4yLDQ1LjUsNDYuMiw0NS4yeiBNNjQuMyw0OC44Yy0xLjQsMS0yLjksMS44LTQuNiwyLjZ2LTkuN2MxLjYtMC43LDMuMi0xLjQsNC42LTIuM1Y0OC44eiBNNTUuOCw1M2MtMS43LDAuNi0zLjYsMS4xLTUuNSwxLjZWNDQuNWMxLjktMC40LDMuOC0wLjksNS41LTEuNFY1M3ogTTM1LjksNTUuOWMtMi4yLTAuMS00LjQtMC4zLTYuNS0wLjZWNDUuMmMyLjEsMC4zLDQuMywwLjUsNi41LDAuNVY1NS45eiBNMjUuNCw1NC42Yy0xLjktMC40LTMuOC0xLTUuNS0xLjZ2LTkuOWMxLjgsMC42LDMuNiwxLDUuNSwxLjRWNTQuNnogTTE1LjksNTEuM2MtMS43LTAuOC0zLjItMS43LTQuNi0yLjd2LTkuM2MxLjQsMC44LDIuOSwxLjYsNC42LDIuM1Y1MS4zeiBNNzEuOCwzNy4xYzAsMi45LTEuMiw1LjctMy41LDguMXYtOC42YzEuMy0xLjEsMi41LTIuMiwzLjUtMy40VjM3LjF6IE0zNy45LDQxLjdDMTkuNSw0MS43LDQsMzMuMSw0LDIyLjhTMTkuNSw0LDM3LjksNHMzMy45LDguNiwzMy45LDE4LjlTNTYuMyw0MS43LDM3LjksNDEuN3oiLz48cGF0aCBkPSJNNDYuOSwxMS41Yy0yLjEtMS4yLTQuNS0xLjgtNy0xLjhjLTUuNiwwLTEwLjUsMy4zLTEyLjYsOGgtMy40Yy0xLjEsMC0yLDAuOS0yLDJzMC45LDIsMiwyaDIuM2MwLDAuNC0wLjEsMC45LTAuMSwxLjNjMCwwLjMsMCwwLjYsMCwwLjloLTIuMmMtMS4xLDAtMiwwLjktMiwyczAuOSwyLDIsMmgzLjJjMiw1LDcsOC41LDEyLjgsOC41YzIuMywwLDQuNS0wLjUsNi41LTEuNmMxLTAuNSwxLjQtMS43LDAuOS0yLjdzLTEuNy0xLjQtMi43LTAuOWMtMS40LDAuNy0zLDEuMS00LjcsMS4xYy0zLjUsMC02LjYtMS44LTguMy00LjVoNi44YzEuMSwwLDItMC45LDItMnMtMC45LTItMi0yaC04LjJjMC0wLjMsMC0wLjYsMC0wLjljMC0wLjUsMC0wLjksMC4xLTEuM2gxMS40YzEuMSwwLDItMC45LDItMnMtMC45LTItMi0yaC05LjhjMS44LTIuNCw0LjctNCw4LTRjMS44LDAsMy41LDAuNSw1LDEuM2MxLDAuNSwyLjIsMC4yLDIuNy0wLjdDNDguMiwxMy4zLDQ3LjgsMTIuMSw0Ni45LDExLjV6Ii8+PC9zdmc+'
		
	));
	
	acf_add_options_page(array(
		'page_title' 	=> '',
		'menu_title'	=> 'Footer',
		'menu_slug'	=> 'footer',
		'position' => 55,
		'icon_url' => 'dashicons-layout'
	));
	acf_add_options_page(array(
		'page_title' 	=> 'Social',
		'menu_title'	=> 'Social',
		'menu_slug'	=> 'social',
		'position' => 58,
		'icon_url' => 'dashicons-share'
	));
}

//Custom toolbars-----------------------
add_filter( 'acf/fields/wysiwyg/toolbars' , 'my_toolbars'  );
function my_toolbars( $toolbars ) {
	$toolbars['Link' ] = array();
	$toolbars['Link' ][1] = array('link' );

	return $toolbars;
}

//Menu children pages----------------------
function menuChildrenPages(){
	$args = array (
		'post_parent' => get_the_ID(),
		'post_type' => 'page',
	);
	$query = new WP_Query( $args ); if ( $query->have_posts() ) {
		echo '<nav id="submenu">';
		while ( $query->have_posts() ) { $query->the_post();
			echo '<a class="anchor-link" href="#'.get_post_field( 'post_name', get_post() ).'">'.get_the_title().'</a>';	
		}
	echo '</nav>';
	} wp_reset_postdata();
}

//Children pages----------------------
function childrenPages(){
	$args = array (
		'post_parent' => get_the_ID(),
		'post_type' => 'page',
	);
	$query = new WP_Query( $args ); if ( $query->have_posts() ) { while ( $query->have_posts() ) { $query->the_post();
			get_template_part('content','module');
	} } wp_reset_postdata();
}

//Checkered------------------------
function checkered($firstImage){
	 $blocs = get_field('blocs',$post->ID);
	if($blocs){
		if($firstImage == 'left'){ $count = 1; } else { $count = 2; } ?>
		<div id="checkered" class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">
		<?php foreach($blocs as $bloc){
			if($count%2 == 0){ $side = 'right'; } else { $side = 'left'; } ?>
		<div class="checkered-row <?= $side; ?>">
			<div class="checkered-block scrollSensible checkered-img"><img src="<?php $img = $bloc['image']; echo $img['sizes']['medium-retina']; ?>"></div>
			<div class="checkered-block scrollSensible checkered-text"><?php echo $bloc['text']; ?></div>
		</div>
		<?php $count++; } ?>
		</div>
	<?php }
}

//PageAnchor-------------------------
function pageAnchor($object){
	if($object->post_parent == 0){
		echo get_permalink($object->ID);
	} else {
		$parentID = $object->post_parent;
		$slug = $object->post_name;
		echo get_permalink($parentID).'#'.$slug;
	}
}

//Diamonds------------------
function diamonds(){ ?>
	<nav id="diamonds" class="container sans">
			<div id="diamonds_left">
				<span class="diamond">
					<span class="diamond-text vertical-align"><?= __('Reservar','lacodinarural'); ?></span>
					<span class="diamond-background"></span>
				</span>
				<a href="<?php pageAnchor(get_field('link_lateral')); ?>">
					<span class="diamond">
						<span class="diamond-text vertical-align"><?php the_field('link_lateral_text'); ?></span>
						<span class="diamond-background"></span>
					</span>
				</a>
			</div>
			<div id="diamonds_right">
				<span id="goTop" class="diamond">
					<span class="diamond-text vertical-align"><img src="<?php bloginfo('template_url'); ?>/library/images/arrow-black.svg"></span>
					<span class="diamond-background"></span>
				</span>
			</div>
		</nav>
<?php }

//VIDEO---------------------------------------------------------------------------------------------------------------
function fullVideo($url, $videoImage, $hasTitle) {
    if (!isset($row)) {
        $row = 1;
    } else {
        $row++;
    }
    $iframe = get_field($url);
    preg_match('/width="(.+?)"/', $iframe, $matches);
    $width = $matches[1];
    preg_match('/height="(.+?)"/', $iframe, $matches);
    $height = $matches[1];

    $ratio = $width / $height;

    preg_match('/src="(.+?)"/', $iframe, $matches);
    $src = $matches[1];
    $src = explode("?", $src);
    $src = $src[0] . '?enablejsapi=1';
    preg_match('/www.(.+?).com/', $iframe, $matches);
    $platform = $matches[1];

    $attributes = 'data-ratio="' . $ratio . '" class="video"';
    $iframe = str_replace('></iframe>', ' ' . $attributes . '></iframe>', $iframe);
    //add_image_size('custom-size-' . $width . '-' . $height, $width, $height, true);
    $image_data = wp_get_attachment_image_src(get_field($videoImage), 'full');
    $image_url = $image_data[0];
    if ($platform == 'youtube') {
        ?>
        <div class="media-wrapper media-wrapper-full media-content media<?= $row ?>" data-width='<?= $width ?>' data-height='<?= $height ?>'>
            <div class='play_youtube playImage' data-videoid="video<?= $row ?>" style='background-image:url(<?= $image_url ?>)'>
                <div class='play'></div>
                <div class="playMask"></div>
        <?php if ($hasTitle) { ?>
                    <div class="video-title">
                        <h1><?php the_title(); ?></h1>
                    </div>
        <?php } ?>
                <div class="darken"></div>
            </div>
            <iframe class="youtubePlayer"  src="<?= $src ?>" frameborder="0" width="780" height="519" allowfullscreen id="video<?= $row ?>"></iframe>
        </div>

    <?php } else { ?>
        <div class="media<?= $row ?> media-wrapper media-wrapper-full media-content vimeoWrapper" data-width='<?= $width ?>' data-height='<?= $height ?>'>
            <div class='play_vimeo playImage' data-videoid="video<?= $row ?>" style='background-image:url(<?= $image_url ?>)'>
                <div class='play'></div>
                <div class="playMask"></div>
        <?php if ($hasTitle) { ?>
                    <div class="video-title">
                        <h1><?php the_title(); ?></h1>
                    </div>
        <?php } ?>
                <div class="darken"></div>
            </div>
            <iframe id="video<?= $row ?>"  class="vimeoPlayer "  src="<?= $src ?>" frameborder="0" width="<?= $width ?>" height="<?= $height ?>" allowfullscreen></iframe>
        </div>

        <?php
    }
}


//END VIDEO----------------------------------------------------------------------------------------------------------
?>

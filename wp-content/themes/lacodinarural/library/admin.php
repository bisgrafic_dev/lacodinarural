<?php
/************* DASHBOARD WIDGETS *****************/

// disable default dashboard widgets
function disable_default_dashboard_widgets() {
	global $wp_meta_boxes;
	// unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);    // Right Now Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);        // Activity Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']); // Comments Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);  // Incoming Links Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);

	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);    // Quick Press Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);     // Recent Drafts Widget
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);           //
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);         //

	// remove plugin dashboard boxes
	unset($wp_meta_boxes['dashboard']['normal']['core']['yoast_db_widget']);           // Yoast's SEO Plugin Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['rg_forms_dashboard']);        // Gravity Forms Plugin Widget
	unset($wp_meta_boxes['dashboard']['normal']['core']['bbp-dashboard-right-now']);   // bbPress Plugin Widget
}

function dashboard_widget() {
	//echo 'This is a custom dashboard widget';
}
function custom_dashboard_widgets() { // calling all custom dashboard widgets
	//wp_add_dashboard_widget( 'dashboard_widget', __( 'Recently on Themble (Customize on admin.php)', 'bonestheme' ), 'dashboard_widget' );
}

add_action( 'wp_dashboard_setup', 'disable_default_dashboard_widgets' );
add_action( 'wp_dashboard_setup', 'custom_dashboard_widgets' );


/************* CUSTOM LOGIN PAGE *****************/

function custom_login_css() {
	wp_enqueue_style( 'custom_login_css', get_template_directory_uri() . '/library/css/login.css', false );
}
function custom_login_url() {  return home_url(); } // changing the logo link from wordpress.org to your site
function custom_login_title() { return get_option( 'blogname' ); } // changing the alt text on the logo to show your site name
// calling it only on the login page
add_action( 'login_enqueue_scripts', 'custom_login_css', 10 );
add_filter( 'login_headerurl', 'custom_login_url' );
add_filter( 'login_headertitle', 'custom_login_title' );


/************* CUSTOMIZE ADMIN *******************/
//Remove menu items
function remove_menus(){
  remove_menu_page( 'index.php' );                  //Dashboard
  remove_menu_page( 'upload.php' );                 //Media
  remove_menu_page( 'edit-comments.php' );          //Comments  
}
add_action( 'admin_menu', 'remove_menus' );

// Custom Backend Footer
function custom_admin_footer() {
	_e( '<span id="footer-thankyou">Developed by <a href="http://www.bisgrafic.com" target="_blank">Bisgràfic</a>.', 'customtheme' );
}
add_filter( 'admin_footer_text', 'custom_admin_footer' );

?>

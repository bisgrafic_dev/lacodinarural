<?php
/*Template Name: El circuit*/
get_header(); ?>
<main>
	<div class="container">
		<h1 id="elcircuit-h1" class="scrollSensible comeFromTop"><?php the_title(); ?></h1>
		<div id="video" class="proportion embed-container" data-numerador="5" data-denominador="3" data-maxHeight="true" style="height: 800px">
			<?php fullVideo('video','video_image',false); ?>
		</div>
		<div id="content" class="scrollSensible comeFromTop col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-12"><?php the_field('content'); ?></div>
		<?php //checkered('right'); ?>
		<?php childrenPages(); ?>
	</div>
</main>

<?php get_footer(); ?>
